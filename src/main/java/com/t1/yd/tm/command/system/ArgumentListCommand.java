package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractSystemCommand {

    public static final String NAME = "arguments";
    public static final String ARGUMENT = "-arg";
    public static final String DESCRIPTION = "Show available arguments";

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");

        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();

        for (final ICommand command : commands) {
            final String arg = command.getArgument();
            if (arg == null || arg.isEmpty()) continue;
            System.out.println(arg);
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
