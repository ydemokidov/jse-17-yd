package com.t1.yd.tm.command.project;

import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project_update_by_id";
    public static final String DESCRIPTION = "Update project by Id";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        final String desc = TerminalUtil.nextLine();

        final Project project = getProjectService().updateById(id, name, desc);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
