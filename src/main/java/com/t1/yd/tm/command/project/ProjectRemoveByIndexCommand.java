package com.t1.yd.tm.command.project;

import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project_remove_by_index";
    public static final String DESCRIPTION = "Remove project by Index";

    @Override
    public void execute() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().removeByIndex(index);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
