package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";
    public static final String ARGUMENT = "-comm";
    public static final String DESCRIPTION = "Show available commands";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");

        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();

        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
