package com.t1.yd.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String taskId, String projectId);

    void removeProjectById(String projectId);

    void unbindTaskFromProject(String taskId, String projectId);

}
